import { ProductService } from "../product/product.service";

export function listing (productSvc: ProductService): any {
  return productSvc.listing().then(response => {
    const { data, meta } = response;
    return { data, meta };
  }).catch(error => {
    return error;
  });
}

export const ListingResolver = {
  token: 'listing',
  deps: [ ProductService ],
  resolveFn: listing
};
