import { Component, OnInit, Input } from '@angular/core';
import { StateService } from '@uirouter/angular';
import { DataRepositoryService } from "../util/data-repository.service";
import { NotificationService } from "../util/notification.service";
import { ProductService } from '../product/product.service';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {

  @Input() listing: any;
  items: Array<any>;
  meta: any;

  constructor (public stateService: StateService, private notification: NotificationService, private productService: ProductService) { }

  ngOnInit() {

    DataRepositoryService._repositoryObservable['Products'].subscribe((data: any) => {
      this.items = [...data];
    });

    this.items = this.listing.data;
    this.meta = this.listing.meta
  }

  loadMore () {
    this.productService.loadPage(this.meta.currentPage + 1)
      .then(response => {
        if (response.status === 'error') {
          return this.notification.showToast({ message: response.message, state: 'error', duration: 3000 }, true);
        }

        this.notification.showToast({ message: 'Items loaded', state: 'success', duration: 3000 }, false);
        this.items = [ ...this.items, ...response.data ];
        this.meta = response.meta;
      })
      .catch(error => {
        if (error.error instanceof Error) {
          this.notification.showToast({ message: "Network error, check your connection", state: 'error', duration: 3000 }, false);
        }
        else {
          const { message } = error.error;
          this.notification.showToast({ message, state: 'error', duration: 3000 }, false);
        }
      });
  }

}
