import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from './router/router.module';
import { UIView } from '@uirouter/angular';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestInterceptor } from "./util/request.interceptor";

import { CurrencyPipe } from '@angular/common';

import { SocketService } from './util/socket.service';
import { DataRepositoryService } from './util/data-repository.service';
import { RequestService } from './util/request.service';
import { ErrorService } from './util/error.service';

import { ProductService } from './product/product.service';

import { AppComponent } from './app.component';
import { ProductComponent } from './product/product.component';
import { DetailComponent } from './detail/detail.component';
import { ListingComponent } from './listing/listing.component';
import { InventoryComponent } from './inventory/inventory.component';
import { ToastComponent } from './toast/toast.component';
import { LoaderComponent } from './loader/loader.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    DetailComponent,
    ListingComponent,
    InventoryComponent,
    ToastComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule, RouterModule, FormsModule, ReactiveFormsModule, HttpClientModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true },
    CurrencyPipe,
    SocketService,
    DataRepositoryService,
    RequestService,
    ErrorService,
    ProductService
  ],
  bootstrap: [ UIView ]
})
export class AppModule { }
