import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../util/notification.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {

  loader: boolean;

  constructor (private notification: NotificationService) { }

  ngOnInit() {
    this.notification.request.subscribe((state: boolean) => {
      this.loader = state;
    });
  }

}
