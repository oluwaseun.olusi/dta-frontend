import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../util/notification.service';

@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss']
})
export class ToastComponent implements OnInit {

  toast: any;
  show: boolean;

  constructor (private notification: NotificationService) { }

  ngOnInit() {
    this.toast = {};

    this.notification.toast.subscribe((state: any) => {
      this.toast = state;
      this.show = true;
      setTimeout(() => {
        this.show = false;
      }, this.toast.duration)
    });
  }

}
