import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  @Input() product: any;

  preview: string;

  constructor() { }

  ngOnInit() {
    this.preview = this.product.featured_media.media_url;
  }

  changePreview (url: string) {
    this.preview = url;
  }

}
