import { ProductService } from "../product/product.service";
import { Transition } from '@uirouter/angular';

export function product (trans: Transition, productSvc: ProductService): any {
  return productSvc.fetchProduct(trans.params()._id).then(response => {
    const { data } = response;
    return data;
  }).catch(error => {
    return error;
  });
}

export const DetailResolver = {
  token: 'product',
  deps: [ Transition, ProductService ],
  resolveFn: product
};

