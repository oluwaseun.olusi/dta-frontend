import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { lodash, number } from "../util/plugins";
import { ProductService } from '../product/product.service';
import { NotificationService } from '../util/notification.service';
import { StateService } from '@uirouter/angular';
import { DataRepositoryService } from '../util/data-repository.service';


@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit {

  @Input() categories: Array<any>;

  @ViewChild("fileInput") fileInput: any;

  category: any;
  product: any;

  colorPicker: any;
  files: Array<any>;

  constructor (
    private productService: ProductService,
    private notification: NotificationService,
    public stateService: StateService
  ) { }

  ngOnInit() {

    DataRepositoryService._repositoryObservable['Categories'].subscribe((data: any) => {
      this.categories = [...data];
    });

    this.reset();
  }

  reset () {
    this.files = [];

    this.category = {
      category_name: null
    };

    this.product = {
      category_id: '',
      name: null,
      description: null,
      price: null,
      tag: [],
      color: []
    };
  }

  selectFile () {
    if (this.fileInput) this.fileInput.nativeElement.click();
  }

  fileChangeEvent (event: any) {
    let files = event.target.files;

    lodash.each(files, file => {
      let newFile: any = {
        file: file,
        fileName: file.name,
        fileSize: number(file.size).format('0.00b'),
        mimeType: file.type,
        filePreview: null
      };

      let fileReader = new FileReader();

      if (file) fileReader.readAsDataURL(file);

      fileReader.addEventListener("load",  function () {
        newFile.filePreview = fileReader.result;
      });

      this.files.push(newFile);
    });
  }

  createCategory () {
    if (!this.category.category_name || this.category.category_name === '') {
      return this.notification.showToast({ message: 'Empty required field(s)', state: 'warning', duration: 3000 }, true);
    }

    this.productService.createCategory(this.category)
      .then(response => {
        if (response.status === 'error') {
          return this.notification.showToast({ message: response.message, state: 'error', duration: 3000 }, true);
        }

        this.notification.showToast({ message: 'Category created', state: 'success', duration: 3000 }, false);
        this.category.category_name = null;
      })
      .catch(error => {
        if (error.error instanceof Error) {
          this.notification.showToast({ message: "Network error, check your connection", state: 'error', duration: 3000 }, false);
        }
        else {
          const { message } = error.error;
          this.notification.showToast({ message, state: 'error', duration: 3000 }, false);
        }
      });
  }

  addTag (event: any) {
    event.preventDefault();
    if (event.which === 13) {
      this.product.tag.push(event.target.value);
      event.target.value = null;
      return false;
    }
  }

  removeTag (tag: string) {
    this.product.tag = this.product.tag.filter(entry => entry !== tag);
  }

  addColor () {
    if (this.colorPicker) {
      if (this.product.color.indexOf(this.colorPicker) < 0) {
        this.product.color.push(this.colorPicker);
      }
      this.colorPicker = null;
      return false;
    }
  }

  removeColor (color: string) {
    this.product.color = this.product.color.filter(entry => entry !== color);
  }

  removeImage (media: any) {
    this.files = this.files.filter(entry => entry.fileName !== media.fileName);
  }

  createProduct () {
    let formData: FormData = new FormData();
    let error = false;

    Object.keys(this.product).map(key => {
      if (!this.product[key] || this.product[key] === '') {
        error = true;
      }

      if (typeof this.product[key] !== 'object') {
        formData.append(key, this.product[key]);
      } else {
        lodash.each(this.product[key], (value, index) => {
          formData.append(`${ key }[${ index }]`, value);
        });
      }
    });

    if (this.files.length > 0) {
      this.files.map(media => {
        formData.append('images', media.file);
      });
    } else {
      return this.notification.showToast({ message: 'Please select at least one image', state: 'warning', duration: 3000 }, true);
    }

    if (error) {
      return this.notification.showToast({ message: 'All fields are required', state: 'warning', duration: 3000 }, true);
    }

    this.productService.createProduct(formData)
      .then(response => {
        if (response.status === 'error') {
          return this.notification.showToast({ message: response.message, state: 'error', duration: 3000 }, true);
        }

        this.notification.showToast({ message: 'Product created', state: 'success', duration: 3000 }, false);
        this.stateService.go('product.detail', { _id: response.data._id }, { reload: true });
      })
      .catch(error => {
        if (error.error instanceof Error) {
          this.notification.showToast({ message: "Network error, check your connection", state: 'error', duration: 3000 }, false);
        }
        else {
          const { message } = error.error;
          this.notification.showToast({ message, state: 'error', duration: 3000 }, false);
        }
      });

  }

}
