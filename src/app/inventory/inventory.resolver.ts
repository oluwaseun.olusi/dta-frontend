import { ProductService } from "../product/product.service";

export function categories (productSvc: ProductService): any {
  return productSvc.categories().then(response => {
    const { data } = response;
    return data;
  }).catch(error => {
    return error;
  });
}

export const CategoryResolver = {
  token: 'categories',
  deps: [ ProductService ],
  resolveFn: categories
};
