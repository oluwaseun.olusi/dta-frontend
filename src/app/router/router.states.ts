import { AppComponent } from "../app.component";
import { ProductComponent } from "../product/product.component";
import { DetailComponent } from "../detail/detail.component";
import { ListingComponent } from "../listing/listing.component";
import { InventoryComponent } from "../inventory/inventory.component";

import { ListingResolver } from '../listing/listing.resolver';
import { CategoryResolver } from '../inventory/inventory.resolver';
import { DetailResolver } from '../detail/detail.resolver';

export const AppRoute = {
  name: 'app',
  redirectTo: '/product',
  component: AppComponent
};

export const ProductRoute = {
  parent: 'app',
  name: 'product',
  url: '/product',
  abstract: true,
  sticky: true,
  deepStateRedirect: true,
  component: ProductComponent,
  resolve: []
};

export const ListingRoute = {
  name: 'product.listing',
  url: '/listing',
  sticky: true,
  deepStateRedirect: true,
  views: {
    'listing@product': {
      component: ListingComponent
    }
  },
  resolve: [ ListingResolver ]
};

export const DetailRoute = {
  name: 'product.detail',
  url: '/detail/:_id',
  sticky: true,
  deepStateRedirect: true,
  views: {
    'detail@product': {
      component: DetailComponent
    }
  },
  resolve: [ DetailResolver ]
};

export const InventoryRoute = {
  name: 'product.inventory',
  url: '/inventory',
  sticky: true,
  deepStateRedirect: true,
  views: {
    'inventory@product': {
      component: InventoryComponent
    }
  },
  resolve: [ CategoryResolver ]
};

export const ROUTER_STATES = [
  AppRoute,
  ProductRoute,
  ListingRoute,
  DetailRoute,
  InventoryRoute
];
