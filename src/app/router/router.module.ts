import { NgModule } from '@angular/core';
import { UIRouterModule } from "@uirouter/angular"
import { ROUTER_STATES } from "./router.states";
import { uiRouterConfigFn } from "./router.config";


@NgModule({
  declarations: [],
  imports: [ UIRouterModule.forRoot({ states: (<any>ROUTER_STATES), useHash: false, config: uiRouterConfigFn }) ],
  exports: [ UIRouterModule ]
})


export class RouterModule { }
