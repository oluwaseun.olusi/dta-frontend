import { UIRouter } from "@uirouter/angular";
import { StickyStatesPlugin } from "@uirouter/sticky-states";


export function uiRouterConfigFn(router: UIRouter) {
  router.plugin(StickyStatesPlugin);

  router.urlService.rules.otherwise({ state: 'product.listing' });
}
