import { Injectable } from '@angular/core';
import { Subject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private requestSubject: Subject<boolean> = new Subject<boolean>();
  request = this.requestSubject.asObservable();

  private toastSubject: Subject<any> = new Subject<any>();
  toast = this.toastSubject.asObservable();

  constructor () { }

  serverAction (state: boolean) {
    this.requestSubject.next(state);
  }

  showToast (state: any, error: boolean): boolean {
    this.toastSubject.next(state);
    return !error;
  }
}
