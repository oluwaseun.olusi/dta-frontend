import * as _ from 'lodash';
import * as numeral from "numeral";

export const lodash = _;
export const number = numeral;
