import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { DataRepositoryService } from "./data-repository.service";
import { ErrorService } from "./error.service";


@Injectable({
  providedIn: 'root'
})
export class RequestService {

  private httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
  private formDataOptions = { headers: new HttpHeaders({ 'enctype': 'multipart/formdata' }) };

  constructor (private http: HttpClient, public repository: DataRepositoryService) { }

  public _get_ (url: string, repository?: string, options?: any) {
    return this.http.get<any>(url, this.httpOptions).toPromise().then(response => {
      if (repository) this.repository.sub(repository, response.data, options);
      return response;
    }).catch(ErrorService.handleHTTPError);
  }

  public _post_ (url: string, body: any, repository?: string) {
    return this.http.post<any>(url, body, this.httpOptions).toPromise().then(response => {
      if (repository) this.repository.sub(repository, response.data);
      return response;
    }).catch(ErrorService.handleHTTPError);
  }

  public _put_ (url: string, body?: any) {
    return this.http.put<any>(url, body, this.httpOptions).toPromise().then(response => {
      return response;
    }).catch(ErrorService.handleHTTPError);
  }

  public _delete_ (url: string) {
    return this.http.delete<any>(url, this.httpOptions).toPromise().then(response => {
      return response;
    }).catch(ErrorService.handleHTTPError);
  }

  public _post_formdata_ (url: string, body: any) {
    return this.http.post<any>(url, body, this.formDataOptions).toPromise().then(response => {
      return response;
    }).catch(ErrorService.handleHTTPError);
  }

  public _put_formdata_ (url: string, body: any) {
    return this.http.put<any>(url, body, this.formDataOptions).toPromise().then(response => {
      return response;
    }).catch(ErrorService.handleHTTPError);
  }
}
