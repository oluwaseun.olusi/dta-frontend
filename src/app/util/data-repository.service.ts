import { Injectable } from '@angular/core';
import { Subject, Observable } from "rxjs";
import { lodash } from './plugins';


@Injectable({
  providedIn: 'root'
})
export class DataRepositoryService {
  static _dataRepository: Array<any> = [];
  static _repositorySubject: Array<Subject<any>> = [];
  static _repositoryObservable: Array<Observable<any>> = [];

  constructor () { }

  sub (repository: string, data: any, options?: any): any {
    if (!DataRepositoryService._repositorySubject.hasOwnProperty(repository) || (options && options.new)) {
      DataRepositoryService._repositorySubject[repository] = new Subject<any>();
      DataRepositoryService._repositoryObservable[repository] = DataRepositoryService._repositorySubject[repository].asObservable();
      DataRepositoryService._dataRepository[repository] = data;
    } else {
      DataRepositoryService._dataRepository[repository] = [ ...DataRepositoryService._dataRepository[repository], ...data ];
    }
  }

  pub (repository: any, data: any, method: string): any {
    if (!DataRepositoryService._repositorySubject.hasOwnProperty(repository)) {
      return [];
    }

    if (method === 'APPEND') {
      DataRepositoryService._dataRepository[repository].push(data);
    }

    if (method === 'REMOVE') {
      DataRepositoryService._dataRepository[repository] = DataRepositoryService._dataRepository[repository].filter(row => !lodash.isEqual(row._id, data._id))
    }

    if (method === 'MUTATE') {
      DataRepositoryService._dataRepository[repository] = DataRepositoryService._dataRepository[repository].map(row => {
        if (lodash.isEqual(row._id, data._id)) {
          row = data;
        }
        return row;
      })
    }

    return DataRepositoryService._repositorySubject[repository].next(DataRepositoryService._dataRepository[repository]);
  }
}
