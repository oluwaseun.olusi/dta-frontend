import * as io from "socket.io-client";
import { Injectable } from '@angular/core';
import { DataRepositoryService } from "./data-repository.service";

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  public static io;

  constructor (public repository: DataRepositoryService) {
    SocketService.io = io.connect(`https://dta-service.herokuapp.com`);

    SocketService.io.on('Data', response => {
      console.log(response);
      if (response.type === 'Create') this.repository.pub(response.repository, response.content, 'APPEND');
      if (response.type === 'Update') this.repository.pub(response.repository, response.content, 'MUTATE');
      if (response.type === 'Delete') this.repository.pub(response.repository, response.content, 'REMOVE');
    });
  }
}
