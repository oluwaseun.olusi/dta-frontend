import { Injectable } from "@angular/core";
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import { finalize, tap } from "rxjs/operators";
import { NotificationService } from "./notification.service";


@Injectable({
  providedIn: 'root'
})
export class RequestInterceptor implements HttpInterceptor {

  constructor (private notification: NotificationService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!(<any>request.headers).async) this.notification.serverAction(true);

    const apiRequest = request.clone({ url: `https://dta-service.herokuapp.com/dta-service/api/v1/${ request.url }` });
    // const apiRequest = request.clone({ url: `http://127.0.0.1:2284/dev/dta-service/api/v1/${ request.url }` });
    let finalRequest = apiRequest;

    return next.handle(finalRequest).pipe(tap((event) => {

    }, (error) => {
      // Do something heres
    }), finalize(() => {
      setTimeout(() => {
        this.notification.serverAction(false);
        if ((<any>request.headers).async) delete (<any>request.headers).async;
      }, 500);
    }));
  }
}
