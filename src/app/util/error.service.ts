import { Injectable } from '@angular/core';
import { HttpErrorResponse } from "@angular/common/http";


@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor() { }

  static handleHTTPError (error: HttpErrorResponse): Promise<any> {
    return Promise.reject(error);
  }
}
