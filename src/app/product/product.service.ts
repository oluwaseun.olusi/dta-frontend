import { Injectable } from '@angular/core';
import { RequestService } from "../util/request.service";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor (private request: RequestService) { }

  listing (): any {
    return this.request._get_(`products`, 'Products');
  }

  loadPage (page: number): any {
    return this.request._get_(`products?page=${ page }`, 'Products');
  }

  categories (): any {
    return this.request._get_(`categories`, 'Categories');
  }

  fetchProduct (id: any): any {
    return this.request._get_(`products/${ id }`);
  }

  createProduct (data: any): any {
    return this.request._post_formdata_(`products`, data);
  }

  createCategory (data: any): any {
    return this.request._post_(`categories`, data);
  }
}
