import { Component } from '@angular/core';
import { SocketService } from "./util/socket.service";

@Component({
  selector: 'app-root',
  template: `<div class="app-content">
    <ui-view></ui-view>
  </div>`,
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor (public socketService: SocketService) {}
}
