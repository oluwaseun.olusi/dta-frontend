"use strict";

const express = require('express');
const path = require('path');

require('dotenv').config();

let app = express();

const forceSSL = () => {
  return function (request, response, next) {
    if (request.headers['x-forwarded-proto'] !== 'https') {
      return response.redirect([ 'https://', request.get('Host'), request.url ].join(''));
    }
    next();
  }
};

if (process.env.NODE_ENV === 'staging') {
  app.use(forceSSL());
}

app.use(express.static(path.join(__dirname, "/../dist/dta-app")));

app.get("*", (request, response) => {
  response.sendFile(path.join(__dirname, "/../dist/dta-app/index.html"));
});

const port = process.env.PORT || process.env.APP_PORT;

app.listen(port, error => {
  if (error) {
    console.error(error);
    return false;
  }

  console.log(`DTA application is running on port ${ port }`);
});
